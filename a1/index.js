

	let trainer = {
			name: "Paul",
			age: 13,
			pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
			friends: {
				hoenn: ["May", "Max"],
				kanto: ["Brock", "Misty"],
			}

	}

	console.log(trainer);


	console.log("Result of dot notation: ")
	console.log(trainer.name);

	console.log("Result of square bracket notation: ");
	console.log(trainer["pokemon"]);

	trainer.talk = function () {
		console.log("Result of talk method:");
  		console.log("Pikachu! I choose you");
	};

	trainer.talk();

	function Pokemon(name, level){
				this.name = name;
				this.level = level;
				// this.health = (level*0.50) + 110;
				// this.attack = (level*10+20)/5;
				this.health = level*2;
				this.attack = level*1.5;
				this.tackle = function(target){

					
					if (this.health <=0){
						console.log("Tried using tackle with " + this.name);
						console.error(this.name + " has 0 health and cannot perform any moves, please choose another pokemon.")
					} else if (this.name == target.name){
						console.log("Tried using tackle with " + this.name);
						console.warn(this.name + " refused to attack itself.");
					} else if (target.health == 0) {
						console.error("Target has already fainted. Please choose another target.");
					} else {	
						console.log(this.name + " tackled " + target.name + ".");

						target.health -= this.attack

						if(target.health<=0){
							target.health = 0;
							console.log(target.name + "'s health is now reduced to " + target.health + ".");
							target.faint()
							console.log(target);
						}else {
							console.log(target.name + "'s health is now reduced to " + target.health + ".");
							console.log(target);
						}
					}
				
				}

				this.faint = function(){
				console.log(this.name + " fainted.")
				}

	}

	let butanding = new Pokemon("Butanding", 22);
			// console.log("Result from creating new pokemon");
			console.log(butanding);

	let bayawak = new Pokemon("Bayawak", 13);
			// console.log("Result from creating new pokemon");
			console.log(bayawak);

	let balyena = new Pokemon("Balyena", 43);
			// console.log("Result from creating new pokemon");
			console.log(balyena);

	bayawak.tackle(balyena);

	bayawak.tackle(butanding);

	butanding.tackle(bayawak);


	function revive (target){
		target.health = target.level*2;
		console.info(target.name + " has been revived to full health!");
		console.log(target);
	}